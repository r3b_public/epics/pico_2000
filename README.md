This is an EPICS driver and IOC for the PICOscope 2000 model range based on the
asynPortDriver class.

Requirements
------------

It is required to set the environment variable 'PICO_SDK' to point to the
installation path of the PICO SDK (including header files and libraries).

This program uses the libps2000a library.
It was tested and used with version 1.1 of this library.

Additional requirements:

- EPICS base
- asyn driver

GUI
---

Included for MEDM in picoApp/adl.
Use the start_gui.sh script.

Copyright Bastian Löher <b.loeher@gsi.de>
